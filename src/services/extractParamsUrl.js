const ExtractParamsUrl = (getString) => {

    getString = getString.slpit('&');
    var result = {};
    getString.forEach((element) => {
        var param = element.slpit('=');
        param[0] = param[0].replace('?', ' ');
        result[param[0]] = param[1];
    });

    return result;
}

export default ExtractParamsUrl;