
import Axios from 'axios';

export const HOST = 'http://localhost:8080';

const HttpServices = async (method, url, data) => {

    let result = {};
    let query;

    if (method === 'GET') {
        query = Axios.get(HOST + url, data);
    } else {
        query = Axios.post(HOST + url, data);
    }

    await query
        .then((res) => {
            result = res.data;
        })
        .catch((e) => {
            result['success'] = false;
            result['errors'] = ['erreur de connexion'];

            console.log(e);
        })

    return result;
}



export default HttpServices;