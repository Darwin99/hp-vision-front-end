import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/js/dist/dropdown';
import 'bootstrap/js/dist/modal';
import App from './components/App';


ReactDOM.render(<App/>, document.getElementById('root'));
