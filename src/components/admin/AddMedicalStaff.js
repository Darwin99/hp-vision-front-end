import { render } from '@testing-library/react';
import  React from 'react';
import { useForm } from 'react-hook-form';
import './../../css/medicalstaff.css';
import Axios from 'axios'
 
import axios from 'axios';

export default class AddHospital extends React.Component 
{state = {
		
	identifiant:"",
	name:"",
	firstName:"",
	birthDate:"",
	CNINumber:"",
	email:"",
	city:"",
	quarter:"",
	fileImage:"",
	tel1:"",
	tel2:"",
	userName:"",
	password:"",
	image:"",
	type:"",
	sex:"",
	status:""
};

changeHandler = (event) =>{
		this.setState({[event.target.name]:event.target.value})
};
handleSubmit = (event) =>
{
	event.preventDefault();
	console.log(this.state);
	Axios.post('/medical',this.state)
	.then(response => {

		console.log(response);
	}).catch(error =>{
		console.log(error);
	});
};
    
render(){
	const {identifiant,name,firstName
		,birthDate,fileImage,CNINumber,email,city,
		quarter,tel1,tel2,userName,password,image,type,sex,status}=this.state;
      return (
			<React.Fragment>
				<form onSubmit = {this.handleSubmit} >
					<h2 className="text-center">Ajout des informations sur la personne</h2>
					<div class="form-row">
						<div class="col">
							<input type="text"value={identifiant}name="identifiant" 
							onChange={this.changeHandler} class="form-control" placeholder="votre identifiant"/>

							<input type="text"value={name} name="name" onChange={this.changeHandler} class="form-control" placeholder="votre nom"/>
							<input type="text"value={firstName} name="firstName" onChange={this.changeHandler} class="form-control" placeholder="prenom du medecin"/>
							<input type="date"value={birthDate}name="birthDate" onChange={this.changeHandler} class="form-control"/>
							<input type="number"value={CNINumber} name="CNINumber" onChange={this.changeHandler} class="form-control" placeholder="numero de la CNI "/>
							<select class="form-select" aria-label="Default select example">
									<option selected>sexe</option>
									<option value="1">Masculin</option>
									<option value="2">Feminin</option>
							</select>
							<input type="file"value={fileImage} name="fileImage" onChange={this.changeHandler} class="form-control"/>
							<select class="form-select" aria-label="Default select example">
									<option selected>status</option>
									<option value="1">One</option>
									<option value="2">Two</option>
									<option value="3">Three</option>
							</select>
							<select class="form-select form-select-lg mb-3" aria-label="Default select example">
									<option selected>Grade</option>
									<option value="1">One</option>
									<option value="2">Two</option>
									<option value="3">Three</option>
							</select>
						</div>
						
 
  
						
						<div class="col">
						<input type="text" value={userName} name="userName" class="form-control" placeholder="Login"/>
							<input type="text" value={password} name="password" onChange={this.changeHandler}class="form-control" placeholder="mot de passe"/>
							<input type="text"value={email} name="email" onChange={this.changeHandler} class="form-control" placeholder="email"/>
							<select class="form-select form-select-lg mb-3" aria-label="Default select example">
									<option selected>type</option>
									<option value="1">patient</option>
									<option value="2">personnel soignant</option>
									 
							</select>
							<input type="text"value={city} name="city" onChange={this.changeHandler} class="form-control" placeholder="ville de residence"/>
							<input type="text"value={quarter} name="quarter" onChange={this.changeHandler} class="form-control" placeholder="quartier"/>
							<input type="text"value={name} onChange={this.changeHandler} class="form-control" placeholder="First name"/>
							<input type="number"value={tel1} name="tel1" onChange={this.changeHandler} class="form-control" placeholder="telephone 1"/>
							<input type="number"value={tel2} name="tel2" onChange={this.changeHandler} class="form-control" placeholder="telephone 1"/>
							<button type="submit" class="btn btn-success">Ajouter</button>
					<button type="submit" class="btn btn-danger">Annuler</button>
						</div>
					</div>
				 
				
				</form>
			</React.Fragment>		 
	)
}
  
}
