 import  React from 'react';
 
 import './../../css/Addhospital.css';
 
import Axios from 'axios';


  export default class AddHospital extends React.Component
{
	state = {
		
		pobox:"",
		email: "",
		name: "",
		fax:"",
		tel1:"",
		tel2:""
	};

	changeHandler = (event) =>{
			this.setState({[event.target.name]:event.target.value})
	};
	handleSubmit = (event) =>
	{
		event.preventDefault();
		console.log(this.state);
		Axios.post('/hospitals',this.state)
		.then(response => {

			console.log(response);
		}).catch(error =>{
			console.log(error);
		});
	};
 
    render()
    {	const {pobox,email,name,fax,tel1,tel2}=this.state;
        return(

            <React.Fragment>
				
					 
 					{/* formulaire de connexion*/}
 					<form onSubmit = {this.handleSubmit} >
 					<h1>Ajout d'un centre medico sanitaire </h1><br/>
 					<fieldset>
 								<legend>Information sur l'hopital</legend>
 						<div className="container formCon">
 							<div className="mb-3">
 								<input type="text" value={name} onChange={this.changeHandler}  name="name"  required   className="form-control" id="codeProfess" placeholder="nom de l'hopital" />
 							</div>
 							<div className="mb-3">
 								<input type="email" value={email}  onChange={this.changeHandler}  name="email"required   className="form-control" id="email" placeholder="email" />
 							</div>

 							<div className="mb-3">
 								<input type="text" required value={tel1}  onChange={this.changeHandler} name="tel1" className="form-control" id="tel1" placeholder="tel1" />
 							</div>

 							<div className="mb-3">
 								<input type="text"  required value={tel2}  onChange={this.changeHandler}name="tel2" className="form-control" id="tel2" placeholder="tel2" />
 							</div>
						
 						</div>
                         <div className="container formCon">
						<div className="mb-3">
 								<input type="text"  required value={pobox}  onChange={this.changeHandler}name="pobox" className="form-control" id="POBox" placeholder="POBox" />
 							</div>
 							<div className="mb-3">
 								<input type="text"  required value={fax}  onChange={this.changeHandler} name="fax" className="form-control" id="fax" placeholder="fax" />
 							</div>

 							<div className="col-12 btnsubmit">
 								<button className="btn btn-success"   type="submit">Connexion</button>
 								<button className="btn btn-primary" type="reset">Annuler</button>
 							</div>
                          </div>
 						</fieldset>
					
  					</form>
				 

 			</React.Fragment>

        )
    }
}
 