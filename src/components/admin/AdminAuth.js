import { render } from '@testing-library/react';
import  React from 'react';
import { useForm } from 'react-hook-form';
import './../../css/Patients.css';
import medical2 from './../../img/medical1.jpg';
import axios from 'axios';

export default function AdminAuth() 
{
    

        return (
			<React.Fragment>
				<div className="container-fluid" id="body">
					{/* image*/}
					<div className="container imageNurse">
						<img src={medical2} />
					</div>
					{/* formulaire de connexion*/}
					<form  >
						<div className="container formCon">
							<h1>Admin login system </h1>
							<div class="mb-3">
								<input type="text"   name="userName" class="form-control" id="codeProfess" placeholder="votre nom utilisateur" />
							</div>
							<div class="mb-3">
								<input type="password"   name="password" class="form-control" id="password" placeholder="mot de passe" />
							</div>

							<div class="col-12 btnsubmit">
								<button class="btn btn-primary" type="submit">Connexion</button>
								<button class="btn btn-primary" type="reset">Annuler</button>
							</div>
							{/*<p>{errors[input.name]?.messages}</p>*/}
						</div>
					</form>
				</div>

			</React.Fragment>

		)
    }
