import React from 'react'
import backGround from './../../img/garde.jpg'
import logo from './../../img/logo.png'
import './../../css/bodyStyles.css'

const NavBar = () =>{
	return(
			<>
			 <img src={backGround} alt="Background" className="background" ></img>
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
				<img src={logo} alt= 'logo' ></img>
			  <a class="navbar-brand" href="#">Elintech</a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>

			    <form class="form-inline my-2 my-lg-0">
			      <input class="form-control mr-sm-2" type="text" placeholder="Search"/>
			      
			    </form>

			  <div class="collapse navbar-collapse" id="navbarColor01">
			    <ul class="navbar-nav mr-auto">
			   
			       
			      <li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
			        <div class="dropdown-menu">
			          <a class="dropdown-item" href="#">Action</a>
			          <a class="dropdown-item" href="#">Another action</a>
			          <a class="dropdown-item" href="#">Something else here</a>
			          <div class="dropdown-divider"></div>
			          <a class="dropdown-item" href="#">Separated link</a>
			        </div>
			      </li>
			    </ul>
			  
			  </div>
			</nav>
			</>


		)
}

export default NavBar;