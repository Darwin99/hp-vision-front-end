import React from 'react'
import img1 from './../../img/image-1.png'
import img2 from './../../img/image-2.png'
import './../../css/MedicalStaffFormStyle.css'
export default function NursingForn(){

	return(
        <div class="wrapper">
			<div class="inner">
                <img src={img1} class="image-1" />
				<form action="">
					<h3>Log in</h3>


					<div class="form-holder">
						<span class="lnr lnr-user"></span>
						<input type="text" class="form-control" placeholder="user name" name="UserName"/>
					</div>

					<div class="form-holder">
						<span class="lnr lnr-envelope"></span>
						<input type="text" class="form-control" placeholder="your mail" name="Login" />
					</div>
					<div class="form-holder">
						<span class="lnr lnr-lock"></span>
						<input type="password" class="form-control" placeholder="Password" name="Password"/>
					</div>
					
					<button>
						<span>Connect</span>
					</button>
				</form>
				<img src={img2} alt="" class="image-2" />
			</div>
			
		</div>

    )

}