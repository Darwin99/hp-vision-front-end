import React from 'react'
import Axios from 'axios'
import InTakeInLoad from '../inTakeInLoad/IntakeInLoad';
import ExtractParamsUrl from '../../services/extractParamsUrl';
import NavBar from './NavBar';

class MedicalNoteBook extends React.Component {
    state = {
        inTakeInLoads: [],
        content: {},
        params: {}
    }

    constructor(props) {
        super(props);

        this.state.params = this.extractUrl(props.location.search)

    }

    extractUrl(getString) {


        getString = getString.split("&")
        var result = {};
        getString.forEach((element) => {
            var param = element.split('=');
            param[0] = param[0].replace('?', ' ');
            result[param[0]] = param[1];
        });

        return result;
    }
    componentDidMount() {

        console.log("PARAMS : ", this.state.params);
        // recovery of inTakeInLoads and the medical note book
        let recoveredNoteBook;
        let recoveredInTakeInLoad;

        Axios.get('/patients/' + this.state.params.patientId + '/medical_note_books/' + this.state.params.id)
            .then((res) => {
                console.log("RESPONSE RECEIVED: ", res.data);
                recoveredNoteBook = res.data;

                Axios.get('/patients/' + this.state.params.patientId + '/medical_note_books/' + this.state.params.id + '/in_take_in_loads')
                    .then((inTakeInLoadsResult) => {
                        recoveredInTakeInLoad = inTakeInLoadsResult.data;
                        console.log("INTAKE : ", inTakeInLoadsResult.data);
                        recoveredInTakeInLoad = this.filteredIntakeInLoad(recoveredInTakeInLoad);
                        this.setState({
                            inTakeInLoads: recoveredInTakeInLoad,
                            content: recoveredNoteBook,
                            params: {}
                        })
                    })


            })
            .catch((err) => {
                console.log("AXIOS ERROR:darwin ", err);
            });
        console.log("INCOM: ", this.state.inTakeInLoads);

    }

    filteredIntakeInLoad(tab) {
        return tab.filter(inTakeInLoad => inTakeInLoad != null)

    }
    render() {


        return (
            <>
                <NavBar />

                { this.state.inTakeInLoads.map((inTakeInLoad) => inTakeInLoad ? <InTakeInLoad inTakeInLoad={inTakeInLoad} /> : "")
                }
            </>
        )
    }
}

export default MedicalNoteBook;