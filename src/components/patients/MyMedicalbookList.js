import React, { Component } from 'react';
import NavBar from './NavBar'
import  './../../img/garde.jpg'
import Axios from 'axios'
import InTakeInLoad from './../inTakeInLoad/IntakeInLoad';

import MedicalNoteBook from './MedicalNotebook'
import './../../css/InTakeInLoad.css'

export default class MyMedicalbookList extends Component {

	state = {
		medicalbookList: [],
		getString: "",
		content: [],
		patient: {},
		user_name: "Msms99"
	}

	

	componentDidMount() {

		let recoveredPatient = {};
		let recoveredBooks;
		Axios.get('/patients/user_name/' + this.state.user_name)
			.then((res) => {
				console.log("RESPONSE RECEIVED: ", res.data);
				this.state.patient = res.data;
				recoveredPatient = res.data;
				Axios.get('/patients/' + recoveredPatient.id + '/medical_note_books')
					.then((medicalbookListResult) => {
 

						console.log("RESPONSE RECEIVED: All medicals note book:************************ ", medicalbookListResult.data);
						recoveredBooks = medicalbookListResult.data;
						this.setState({
							medicalbookList: medicalbookListResult.data,
							patient: recoveredPatient
						})

					})
					.catch((err) => {
						console.log("AXIOS ERROR:darwin ", err);
					});


			})
			.catch((err) => {
				console.log("AXIOS ERROR:darwin ", err);
			});

		


	}

	render() {


		return (
			<>




				<NavBar />

				<div className="medicalNoteBookList">




					{


						this.state.medicalbookList.map((medicalNoteBook) => {


							return (
								<>

								
								
								<div className="medicalNoteBookLine">
									<h1>Medical notebook of:   {medicalNoteBook.creationDate}  </h1>
									<button className="btn btn-prinary showBtn" ><a href={"/medicalNoteBook?test=12&id=" + medicalNoteBook.id + '&patientId='+this.state.patient.id} >                    Show</a></button>

								</div>

								</>
							)

						}
						)
					}

				</div>
			</>

		)
	}
}

