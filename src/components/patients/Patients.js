import { render } from '@testing-library/react';
import React, { useState } from 'react';
import './../../css/Patients.css';
import medical2 from './../../img/medical1.jpg';
import service from './../../services/Services';
import Axios from 'axios';


export default class Patients extends React.Component {
		state = {
			user_name: "",
			password: ""

		};

	 handleSubmitForm =(e)=> {
		 e.preventDefault();
		//first we have to recovery consultions on this in take in load

		 
			Axios.get('/patients/user_name/'+ this.state.user_name)
				.then((res) => {
					var recuvedPassword   = res.data.password
					if(recuvedPassword === this.state.password)
					{
						this.props.history.push('/MyMedicalbookList?test=12&user_name='+res.data.userName);
					}else{
						console.log("CONNEXION_ERROR:");
					}
					console.log("Patient",res.data);
				})
				.catch((err) => {
					console.log("AXIOS_ERROR:akhira ", err);
				});
			}
		render()
		{
			return (
				<React.Fragment>
					<div className="container-fluid" id="body">
						{/* image*/}
						<div className="container imageNurse">
							<img src={medical2} alt="image personnel medical" />
						</div>
						{/* formulaire de connexion*/}
						<form onSubmit = {(event)=>this.handleSubmitForm(event)}>
							<div className="container formCon">
								<h1>users patients login</h1>
								<div className="mb-3">
									<input type="text" required name="user_name" 
									  onChange={e=>this.setState({user_name:e.target.value})}
										className="form-control" id="codeProfess" placeholder="votre nom utilisateur" />
								</div>
								<div className="mb-3">
									<input type="password"
										  onChange={e=>this.setState({password:e.target.value})}
										name="password" className="form-control" required id="password" placeholder="mot de passe" />
								</div>
 
								<div className="col-12 btnsubmit">
									<button className="btn btn-primary" type="submit">Connexion</button>
									<button className="btn btn-primary" type="reset">Annuler</button>
								</div>
								{/*<p>{errors[input.name]?.messages}</p>*/}
							</div>
						</form>
					</div>

				</React.Fragment>

			)
		}

	}
