import React from 'react';
import Axios from 'axios';

import HttpServices from './../services/HttpServices';
import Patients from './patients/Patients'
import MyMedicalbookList from './patients/MyMedicalbookList'

import NursingForn from './starffs/NursingForn'
import AdminAuth from './admin/AdminAuth'
import Dashboad from './admin/Dashboad'
import AddHospital from './admin/AddHospital'
import AddMedicalStaff from './admin/AddMedicalStaff'
import CheckMedicalStarff from './starffs/CheckMedicalStarff'
import IntakeInLoad from './inTakeInLoad/IntakeInLoad'
import Home from './Home'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Axios from 'axios'
import Fetching from './Fetching'
import MedicalNoteBook from './patients/MedicalNotebook'




class App extends React.Component {


	state = {
		fetching: true
	} 

	componentDidMount() {

		let delay = Math.floor(Math.random() * 5000)

	// setTimeout(()=>{
	// 	this.setState({
	// 		fetching : false
	// 	})
	// },delay)

	}



	render() {

		return (
			<section>
				{/* {this.state.fetching? <Fetching /> : null} */}

				{/* construction du routeur*/}
				<BrowserRouter>
					<Switch>
					
					<Route path='/medicalNoteBook' render={(props) => <MedicalNoteBook {...props} />} />
						<Route path='/MyMedicalbookList' component={MyMedicalbookList} />
						<Route path='/Medical_starff_loggin' component={NursingForn} />
						<Route path='/starffs/NursingForn' component={NursingForn} />
						<Route path='/admin_loggin' component={AdminAuth} />
						<Route path='./admin/AdminAuth' component={AdminAuth} />
						<Route path='/admin_dashboad' component={Dashboad} />
						<Route path='./admin/Patient' component={Patients} />
						<Route path='/AddHospital' component={AddHospital} />
						<Route path='/AddMedicalStaff' component={AddMedicalStaff} />
						<Route path='/check_medical_notebook' component={CheckMedicalStarff} />
						<Route path='/in_take_in_load' component={IntakeInLoad} />
						<Route path='/' component={Patients} />



					</Switch>
				</BrowserRouter>


			</section>


		)




	}
}

export default App;