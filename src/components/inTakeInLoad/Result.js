import React from 'react'

class Result extends React.Component {

state = {
    id : this.props.content.id,
    label: this.props.content.label,
    medicalStaff: this.props.content.medicalStaff,
    date: this.props.content.date
}

    render() {
        return (
            <>
                <h2>Result : {this.state.date}</h2>
                    {this.state.label}
                <div className="relatedMedicalStaff">
                <h3>Medical staff</h3>
                    <div className="medicalStaffName"> {this.state.medicalStaff.type} {this.state.medicalStaff.firstName} {this.state.medicalStaff.name} </div>
                    <div className="medicalStaffType">{this.state.medicalStaff.type}</div>
                    <div className="number">Tel: {this.state.medicalStaff.tel1}</div>

                </div>
            </>
        )
    }
}

export default Result;