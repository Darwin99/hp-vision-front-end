import React from 'react'


class Consultation extends React.Component{

    state ={
        temparature : this.props.content.temparature,
        weight : this.props.content.weight,
        tesion : this.props.content.tesion,
        synptoms : this.props.content.synptoms,
        diagnostic : this.props.content.diagnostic,
        date : this.props.content.date,
        hour : null,
        medicalStaff: this.props.content.medicalStaff
    }

    render(){
        return (
             <div className="consultation">
                <h1>Consultation  : {this.state.date} </h1>
                <div className="consultationContent">
                    <div className="temparature">
                        <h2>temparature</h2>
                        {this.state.temparature}
                    </div>
                    <div className="weight">
                        <h2>Weight</h2>
                        {this.state.weight}
                    </div>
                    <div className="tesion">
                        <h2>tesion</h2>
                        {this.state.tesion}
                    </div>

                    <div className="symptom">
                        <h2>symptom</h2>
                        {this.state.synptoms}
                    </div>

                    <div className="diagnostic">
                        <h2>diagnostic</h2>
                        {this.state.diagnostic}
                    </div>
                </div>

                {/* <div className="relatedMedicalStaff">
                    <div className="medicalStaffName">  {this.state.medicalStaff.grade} {this.state.medicalStaff.firstName} { this.state.medicalStaff.name }  </div>
                    <div className="medicalStaffType">{this.state.medicalStaff.grade}</div>
                    <div className="number"> {this.state.medicalStaff.tel1}</div>

                </div> */}
            </div>
        )
    }
}

export default Consultation;