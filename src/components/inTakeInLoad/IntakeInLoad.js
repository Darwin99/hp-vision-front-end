import React from 'react'
// import './../../css/inTakeInLoad.css'
import Examen from './Examen';
import Prescription from './Prescription'
import Consultation from './Consultation'
import Axios from 'axios'



class InTakeInLoad extends React.Component {

    state = {
        content: {},
        consultations: [],
        examens: [],
        prescriptions: []
    }

    constructor(props) {

        super(props);
        this.state.content = props.inTakeInLoad;
    }

    componentDidMount() {
        //first we have to recovery consultions on this in take in load

        let recoverdConsultations;
        let recoveredExamens;
        let recoverdPrescriptions;


        // console.log("CONTENT !!!!!!: ", this.state.content);
        // let val = this.state.content.medicalNoteBook;
        console.log("******Medical note book *******: ", this.state.content.medicalNoteBook.id);


        Axios.get('/patients/' + this.state.content.medicalNoteBook.patient.id + '/medical_note_books/' + this.state.content.medicalNoteBook.id + '/in_take_in_loads/' + this.state.content.id + '/consultations')
            .then((res) => {
                console.log("RESPONSE RECEIVED: ", res.data);
                this.state.consultations = res.data;
                recoverdConsultations = res.data;
                //Recovery of examens
                Axios.get('/patients/' + this.state.content.medicalNoteBook.patient.id + '/medical_note_books/' + this.state.content.medicalNoteBook.id + '/in_take_in_loads/' + this.state.content.id + '/examens')
                    .then((ExamenResult) => {
                        console.log("RESPONSE ExamenResult: ", ExamenResult.data);
                        this.state.examens = ExamenResult.data;
                        recoveredExamens = ExamenResult.data;

                        //Recovery of prescriptions
                        Axios.get('/patients/' + this.state.content.medicalNoteBook.patient.id + '/medical_note_books/' + this.state.content.medicalNoteBook.id + '/in_take_in_loads/' + this.state.content.id + '/prescriptions')
                            .then((prescriptionsResult) => {
                                console.log("RESPONSE prescriptionsResult: ", prescriptionsResult.data);
                                this.state.prescriptions = prescriptionsResult.data;
                                recoverdPrescriptions = prescriptionsResult.data;

                                this.setState({
                                    content: this.state.content,
                                    consultations: recoverdConsultations,
                                    examens: recoveredExamens,
                                    prescriptions: recoverdPrescriptions
                                })
                            })
                            .catch((err) => {
                                console.log("AXIOS ERROR:darwin ", err);
                            });
                    })
                    .catch((err) => {
                        console.log("AXIOS ERROR:darwin ", err);
                    });


            })
            .catch((err) => {
                console.log("AXIOS ERROR:darwin ", err);
            });





        //After dat recovery, we have to show the inTake inLoad

    }

    render() {

        console.log("Consultations: ****", this.state.consultations);
        return (
            <div className="inTakeInLoad">
                {
                    this.state.consultations.map((consultation) => <Consultation content={consultation} />)

                }

<<<<<<< HEAD
                     this.sate.prescriptions.map((prescription) => <Prescription content={prescription} />)
=======
                {
                    this.state.examens.map((examen) => <Examen content={examen}   />)
                }
                {
                    this.state.prescriptions.map((prescription) => <Prescription content={prescription} />)
>>>>>>> 4c8d4fa35ba2baec1360859d24ec0be87334559f

                }




                {/* <div classclass="hospital">Hospital: {this.state.content.hospital.name}</div> */}
            </div>
        )
    }
}

export default InTakeInLoad;