import React from 'react';
import './../css/home.css';
import { NavLink } from 'react-router-dom';
import img4 from "./../img/medicale4.jpg"
import img5 from "./../img/medicale5.jpg"
import medical from "./../img/medecine8.jpg"
export default function Home(){

    return(

        <>
        <div className="connexionImages">

            <div className="connexionImage">
                <NavLink to='./starffs/NursingForn'>
                    <img src={img5}/>
                    <p>MedicaleStaff</p>
                </NavLink>

            </div>


            <div className="connexionImag">
            <NavLink to='./admin/Patient'>
                    <img src={medical}/>
                    <p>Patient</p>
                </NavLink>
            </div>

            <div className="connexionImage">
             <NavLink to='./admin/AdminAuth'>
                    <img src={medical}/>
                    <p>Administrateur</p>
                </NavLink>

             </div>
        </div>
        </>
    )
}